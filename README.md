# Chorebot

Chorebot is a Drake Cooper labs experiment, using Google sheets and the Slack API to build a bot to randomly select and assign Slack users to predefined chores.

## Getting Started

1. Create a new spreadsheet in Google Sheets. It needs 2 sheets, "people" and "chores".
2. Populate the first column of the "people" sheet with Slack usernames, and the first column of "chores" with the chores you want to assign.
3. Open the Script Editor (Tools > Script editor...) and copy/paste the code in `chorebot.js`.
4. In Slack, under your organization name, choose "Apps & Custom Integrations", then choose "configure". Under "Custom Integrations", configure a new incoming web hook.
5. Copy/paste your Webhook URL into the `settings` variable in the script editor.
6. Test it out by selecting the `delegateChores()` function in the script editor, then clicking "run" (play button).
7. Add some triggers in the script editor to run the script automatically.
8. Build on chorebot by adding your own custom functions.

## Notes

+ Only give your Google Sheet access as-needed, as you will be storing a somewhat private Webhook URL there. If, somehow your Webhook URL gets compromised, you can always regenerate it in Slack.
