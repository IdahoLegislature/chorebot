function delegateChores() {

  var settings = {
    slackWebhookURL: 'https://hooks.slack.com/services/XXXXXXXXX/YYYYYYYYY/ZZZZZZZZZZZZZZZZZZZZZZZZ'
  };

  // Get the spreadsheet
  var ss = SpreadsheetApp.getActiveSpreadsheet();

  // Get the list of people
  var s_people = ss.getSheetByName('people');
  var people = s_people.getDataRange().getValues();

  // Get the list of chores
  var s_chores = ss.getSheetByName('chores');
  var chores = s_chores.getDataRange().getValues();

  // Set up an array to store already assigned people
  var assigned = [];

  // Get the current date
  var today = new Date();
  var dayOfWeek = today.getDay();
  var dayOfMonth = today.getDate();

  // Return a random person index that hasn't been chosen yet
  function uniqueRandPerson(arr) {
    var e;

    // Only run this loop if there are still people left to choose from
    if(arr.length < people.length) {
      do {
        e = Math.floor(Math.random() * people.length);
        Logger.log(e);
      } while (arr.indexOf(e) !== -1);
    } else {

      // Return -1 if everyone has already been chosen
      e = -1;
    }

    return e;
  }

  // Loop through the chores, everything needs to get done!
  for(var i = 0; i < chores.length; i++) {

    // Grab a randome person index
    var rand_person = uniqueRandPerson(assigned);

    // If the chore is blank, move along
    if(chores[i][0] === '') {
      continue;
    }

    if(rand_person !== -1) {

      // Assign the lucky winner to the current chore, if it matches the correct frequency
      if(chores[i][1] === 'd' || ( chores[i][1] === 'w' && dayOfWeek === 1 ) || ( chores[i][1] === 'm' && dayOfMonth < 7 && dayOfWeek === 1) ) {

        assign(people[rand_person][0], chores[i][0]);

        // Add the assignee to the assigned array so they don't get chosen again
        assigned.push(rand_person);
      }

    } else {

      Logger.log('There are more chores than people!');

    }
  }

}

var payload = {
  'link_names': 1
}

var opts = {
  'method': 'post',
  'contentType': 'application/json'
}

// Send a message to the #chores slack channel, assigning a @user to a chore.
function assign(user, chore) {

  var msg = user + ', ' + chore;
  talk(msg);

}

// Utility fn to send an ad hoc message as Rosie
function talk(msg) {

  payload.text = msg;
  opts.payload = JSON.stringify(payload);

  Logger.log(payload.text);

  result = UrlFetchApp.fetch(settings.slackWebhookURL, opts);

}
